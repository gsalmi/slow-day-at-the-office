(function(){
    'use strict'

    // constants:
    var TILE_SIZE = 64;

    var WORLD_WIDTH_TILES = 14;
    var WORLD_HEIGHT_TILES = 10;
    var WORLD_TILE_COUNT = WORLD_WIDTH_TILES * WORLD_HEIGHT_TILES;
    var WORLD_WIDTH_WORLDSPACE = WORLD_WIDTH_TILES * TILE_SIZE;
    var WORLD_HEIGHT_WORLDSPACE = WORLD_HEIGHT_TILES * TILE_SIZE;

    var TILE_TYPE_EMPTY = 0;
    var TILE_TYPE_STONE = 1;

    var PLAYER_COLLISION_RADIUS = 20;

    var WEAPON_KEYBOARD = 1;

    // globals:
    var currentWorld = null;
    var app = new PIXI.Application(WORLD_WIDTH_WORLDSPACE, WORLD_HEIGHT_WORLDSPACE, {backgroundColor : 0x0});
    document.getElementById('gameArea').appendChild(app.view);

    var TEXTURE_PLAYER = PIXI.Texture.fromImage('gfx/player.png');
    var TEXTURE_PLAYER_DOWN = PIXI.Texture.fromImage('gfx/player_down.png');
    var TEXTURE_PLAYER_KB = PIXI.Texture.fromImage('gfx/player_kb_idle.png');
    var TEXTURE_PLAYER_HIT = PIXI.Texture.fromImage('gfx/player_kb_hit.png');

    var TEXTURE_KEY = PIXI.Texture.fromImage('gfx/key.png');
    var TEXTURE_MARKER = PIXI.Texture.fromImage('gfx/marker.png');

    function tilePosToWorld(pos) {
        return pos * TILE_SIZE; 
    }

    function tilePosToWorldCentered(pos) {
        return pos * TILE_SIZE + 0.5*TILE_SIZE; 
    }

    function ensure(condition, message) {
        if (!condition) {
            console.log('ensure failed: ' + message);
        }
    }

    function vec2Length(x, y) {
        return Math.sqrt(x*x + y*y);
    }

    function vec2Dist(x1, y1, x2, y2) {
        return vec2Length(x1-x2, y1-y2);
    }

    function tileCoordsToIndex(x, y) {
        ensure(x >= 0 && x < WORLD_WIDTH_TILES, 'invalid x-coordinate');
        ensure(y >= 0 && y < WORLD_HEIGHT_TILES, 'invalid y-coordinate');

        return y*WORLD_WIDTH_TILES + x;
    }

    function tileIndexToWorldX(index) {
        ensure(index >= 0 && index < WORLD_TILE_COUNT, 'invalid index');
        return TILE_SIZE * (index % WORLD_WIDTH_TILES);
    }

    function tileIndexToWorldY(index)
    {
        ensure(index >= 0 && index < WORLD_TILE_COUNT, 'invalid index');
        return TILE_SIZE * Math.floor(index / WORLD_WIDTH_TILES);
    }

    function worldToTile(pos) {
        return pos / TILE_SIZE;
    }

    function rangeLimit(val, min, max) {
        val = val > min ? val : min;
        val = val < max ? val : max;
        return val;
    }

    function pickClosest(val, a, b) {
        if (Math.abs(val - a) > Math.abs(val - b))
            return a;
        else
            return b;
    }

    function onSoundLoaded() {
    }

    function playSound(path, vol) {
        sound.playEx(path, vol, 1, false);
    }

    var SOUND_HIT = './sfx/hit';
    var SOUND_PICKUP = './sfx/pickup';
    var SOUND_STONE = './sfx/stone';
    var SOUND_LEVEL_CLEARED = './sfx/level_cleared';
    var SOUND_GAME_OVER = './sfx/game_over';

    sound.init();
    sound.load(SOUND_HIT, onSoundLoaded);
    sound.load(SOUND_PICKUP, onSoundLoaded);
    sound.load(SOUND_STONE, onSoundLoaded);
    sound.load(SOUND_LEVEL_CLEARED, onSoundLoaded);
    sound.load(SOUND_GAME_OVER, onSoundLoaded);

    var currentStatusText = '';
    function setStatusText(s) {
        if (s !== currentStatusText) {
            currentStatusText = s;
            document.getElementById('statusText').innerHTML = s;
        }
    }

    function addStone(world, tileX, tileY) {
        var worldX = tileIndexToWorldX(tileX);
        var worldY = tileIndexToWorldX(tileY);
        var tileIndex = tileCoordsToIndex(tileX, tileY);

        var stoneSprite = PIXI.Sprite.fromImage('./gfx/stone.png', true);
        stoneSprite.x = worldX;
        stoneSprite.y = worldY;
        
        world.tileMap[tileIndex] = TILE_TYPE_STONE;
        world.root.addChild(stoneSprite);
    }

    function playerStaticRectCollision(player, x, y, w, h) {
        if (!player.alive ||
            player.root.x + PLAYER_COLLISION_RADIUS < x ||
            player.root.x - PLAYER_COLLISION_RADIUS > x + w ||
            player.root.y + PLAYER_COLLISION_RADIUS < y ||
            player.root.y - PLAYER_COLLISION_RADIUS > y + h) return;

        var closestInRectX = rangeLimit(player.root.x, x, x+w);
        var closestInRectY = rangeLimit(player.root.y, y, y+h);

        var toPlayerCenterX = player.root.x - closestInRectX;
        var toPlayerCenterY = player.root.y - closestInRectY;
        var dist = vec2Length(toPlayerCenterX, toPlayerCenterY);
        if (dist > PLAYER_COLLISION_RADIUS)
            return;

        var depth = PLAYER_COLLISION_RADIUS-dist;
        player.root.x += depth*toPlayerCenterX/dist;
        player.root.y += depth*toPlayerCenterY/dist;
    }

    function createWorld() {
        var root = new PIXI.Sprite();
        root.interactive = true;
        
        var floorLayer = new PIXI.Sprite();
        root.addChild(floorLayer);

        var deskLayer = new PIXI.Sprite();
        root.addChild(deskLayer);

        var keyboardLayer = new PIXI.Sprite();
        root.addChild(keyboardLayer);

        var particleLayer = new PIXI.Sprite();
        root.addChild(particleLayer);

        var playerLayer = new PIXI.Sprite();
        root.addChild(playerLayer);

        var wallLayer = new PIXI.Sprite();
        root.addChild(wallLayer);

        


        var tileMap = [];

        for (var i = 0; i < WORLD_TILE_COUNT; i++)
        {
            tileMap.push(TILE_TYPE_EMPTY);

            var floorTile = PIXI.Sprite.fromImage('gfx/floor.png');
            floorTile.x = tileIndexToWorldX(i);
            floorTile.y = tileIndexToWorldY(i);
            floorLayer.addChild(floorTile);
        }
        
        var world = {
            root: root,
            tileMap: tileMap,
            players: [],
            walls: [],
            desks: [],
            keyboards: [],
            particles: [],
            level: 1,

            shakeTimer: 0,
            shakeStrength: 0,

            lastMouseX: 0,
            lastMouseY: 0,
            collisionDetails: null,

            stoneSpawnDelaySecs: 1.0,
            nextStoneSpawnTimer: 1.0,
            stoneSpawnOffset: 1,
            stoneSpawnDir: 0,
            stoneSpawnX: 1,
            stoneSpawnY: 1,

            levelStartTimer: 3,
            levelEndTimer: 2,

            gameOver: false,
            levelCleared: false,

            init: function() {
                this.root.pointerdown = this.onMouseDown.bind(this);
                this.root.pointermove = this.onMouseMove.bind(this);

                this.root.interactive = true;
            },
            destroy: function() {
                root.destroy({children:true, texture: false, baseTexture: false});
            },
            addPlayer: function(p) {
                this.players.push(p);

                if (!p.userControlled) {
                    p.health -= 2;
                }

                playerLayer.addChild(p.root);
            },
            moveToFloorLayer: function(s) {
                floorLayer.addChild(s);
            },
            addWall: function(w) {
                this.walls.push(w);

                wallLayer.addChild(w.root);
            },
            addDesk: function(w) {
                this.desks.push(w);

                deskLayer.addChild(w.root);
            },
            addKeyboard: function(k) {
                this.keyboards.push(k);

                keyboardLayer.addChild(k.root);
            },
            addParticle: function(k) {
                this.particles.push(k);

                particleLayer.addChild(k.root);
            },
            forAllPlayersOnTile(x, y, fn) {
                var minWorldX = x * TILE_SIZE;
                var maxWorldX = minWorldX + TILE_SIZE;
                var minWorldY = y * TILE_SIZE;
                var maxWorldY = minWorldY + TILE_SIZE;

                for (var i in this.players) {
                    var player = this.players[i];

                    if (player.root.x > minWorldX &&
                        player.root.x < maxWorldX &&
                        player.root.y > minWorldY &&
                        player.root.y < maxWorldY)
                    {
                        fn(player);
                    }
                }
            },
            update: function(dt) {
                for (var i in this.particles) {
                    this.particles[i].update(dt);
                }

                if (this.shakeTimer > 0) {
                    this.shakeTimer -= dt;

                    var max = this.shakeTimer*this.shakeTimer*this.shakeStrength;
                    this.root.x = max*(Math.random()-0.5);
                    this.root.y = max*(Math.random()-0.5);
                } 
                else {
                    this.root.x = 0;
                    this.root.y = 0;
                }

                if (this.gameOver) {
                    setStatusText('GAME OVER - PRESS R TO RESTART');
                    if (input.keyPressedNow(input.K_R)) {
                        setupLevel(this.levelNum);
                    }
                    return;
                }

                if (this.levelCleared) {
                    setStatusText('LEVEL CLEARED!');

                    if (this.levelEndTimer > 0)
                        this.levelEndTimer -= dt;
                    else
                        setupLevel(this.levelNum+1);
                    
                    return;
                }

                if (this.levelStartTimer > 0) {
                    setStatusText('GET READY, STARTING IN ' + Math.ceil(this.levelStartTimer));

                    this.levelStartTimer -= dt;
                    return;
                }

                setStatusText('WASD - Move');

                var anyOpponentLeft = false;
                for (var i in this.players) {
                    var player = this.players[i];
                    player.update(dt);

                    if (player.userControlled && !player.alive) {
                        this.gameOver = true;
                        console.log("Game over!");

                        playSound(SOUND_GAME_OVER, 1);
                    }

                    if (!player.userControlled && player.alive) {
                        anyOpponentLeft = true;
                    }
                }

                if (!anyOpponentLeft && !this.levelCleared) {
                    this.levelCleared = true;
                    playSound(SOUND_LEVEL_CLEARED, 1);
                }

                // TILEMAP -> player collisions
                for (var i in this.tileMap) {
                    var tileType = this.tileMap[i];
                    if (tileType === TILE_TYPE_EMPTY)  
                        continue;

                    var tileX = tileIndexToWorldX(i);
                    var tileY = tileIndexToWorldY(i);

                    for (var playerIndex in this.players) {
                        var player = this.players[playerIndex];
                        playerStaticRectCollision(player, tileX, tileY, TILE_SIZE, TILE_SIZE);
                    }

                    
                }

                // wall -> player collisions
                for (var i in this.walls) {
                    var wall = this.walls[i];

                    for (var playerIndex in this.players) {
                        var player = this.players[playerIndex];
                        playerStaticRectCollision(player, wall.x, wall.y, wall.width, wall.height);
                    }
                }

                // update desks
                for (var i in this.desks) {
                    this.desks[i].update(dt);
                }

                // spawn new stones
                this.nextStoneSpawnTimer -= dt;
                if (this.nextStoneSpawnTimer < 0) {
                    this.nextStoneSpawnTimer += this.stoneSpawnDelaySecs;

                    this.forAllPlayersOnTile(this.stoneSpawnX, this.stoneSpawnY, function deathByStoneSpawn(player) {
                        player.onDown();
                    });

                    addStone(this, this.stoneSpawnX, this.stoneSpawnY);
                    playSound(SOUND_STONE, 0.6);

                    this.shakeTimer = 0.1;
                    this.shakeStrength = 1000;

                    var rowDone = false;

                    switch (this.stoneSpawnDir) {
                        case 0:
                            if (this.stoneSpawnX == WORLD_WIDTH_TILES - this.stoneSpawnOffset - 1) {
                                this.stoneSpawnY++;
                                this.stoneSpawnDir += 1;
                            } 
                            else {
                                this.stoneSpawnX++;
                            }
                            break;
                        case 1:
                            if (this.stoneSpawnY == WORLD_HEIGHT_TILES - this.stoneSpawnOffset - 1) {
                                this.stoneSpawnX--;
                                this.stoneSpawnDir += 1;
                            } 
                            else {
                                this.stoneSpawnY++;
                            }
                            break;
                        case 2:
                            if (this.stoneSpawnX == this.stoneSpawnOffset) {
                                this.stoneSpawnY--;
                                this.stoneSpawnDir += 1;
                            } 
                            else {
                                this.stoneSpawnX--;
                            }
                            break;
                        case 3:
                            if (this.stoneSpawnY == this.stoneSpawnOffset+1) {
                                this.stoneSpawnX++;
                                this.stoneSpawnOffset++;
                                this.stoneSpawnDir = 0;
                            } 
                            else {
                                this.stoneSpawnY--;
                            }
                            break;
                    }
                }
            },
            onMouseMove: function(evt) {
                evt.stopPropagation();

                var pos = evt.data.getLocalPosition(root);

                this.lastMouseX = pos.x;
                this.lastMouseY = pos.y;

                //console.log('mouse move: ' + pos.x + ', ' + pos.y);
            },
            onMouseDown: function(evt) {
                evt.stopPropagation();

                var pos = evt.data.getLocalPosition(root);

                for (var i in this.players) {
                    var player = this.players[i];
                    if (player.userControlled) {
                        player.useWeapon();
                    }
                }

                //console.log('mouse down: ' + pos.x + ', ' + pos.y);
            }
        };

        world.init();
        return world;
    }

    function createKeyboard(worldX, worldY, rot) {
        var root = PIXI.Sprite.fromImage('./gfx/keyboard_flat.png');
        root.anchor.x = 0.5;
        root.anchor.y = 0.5;
        root.rotation = 0.5*Math.PI*rot;

        root.x = worldX;
        root.y = worldY;

        return {
            root: root
        }
    }

    function createKey(worldX, worldY) {
        var root = new PIXI.Sprite();
        root.texture = TEXTURE_KEY;
        root.anchor.x = 0.5;
        root.anchor.y = 0.5;
        root.x = worldX;
        root.y = worldY;

        var rot = Math.random()*2*Math.PI;
        var speed = Math.random()*250;
        var velX = Math.cos(rot)*speed;
        var velY = Math.sin(rot)*speed;

        return {
            root: root,
            timer: 1.0,
            update(dt) {
                if (this.timer <= 0)
                    return;

                this.timer -= dt;

                root.x += velX*this.timer*this.timer*dt;
                root.y += velY*this.timer*this.timer*dt;
            }
        }
    }

    function createDesk(tileX, tileY, rot) {
        var root = PIXI.Sprite.fromImage('./gfx/desk.png');
        root.anchor.x = 0.5;
        root.anchor.y = 0.5;
        root.rotation = 0.5*Math.PI*rot;

        root.x = tilePosToWorldCentered(tileX);
        root.y = tilePosToWorldCentered(tileY);

        return {
            root: root,
            update: function(dt) {
                for (var i in currentWorld.players) {
                    // desk -> player collisions:
                    var bounds = root.getBounds();
                    playerStaticRectCollision(currentWorld.players[i], bounds.x, bounds.y, bounds.width, bounds.height);
                }
            }
        }
    }

    function createPlayer(userControlled, spawnX, spawnY) {
        var root = new PIXI.Sprite();
        root.x = spawnX;
        root.y = spawnY;

        var body = new PIXI.Sprite.fromImage('./gfx/player.png');
        body.anchor.x = 0.5;
        body.anchor.y = 0.5;
        root.addChild(body);

        if (userControlled) {
            var marker = new PIXI.Sprite.fromImage('./gfx/marker.png');
            marker.anchor.x = 0.5;
            marker.anchor.y = 0.5;
            marker.alpha = 0.5;
            
            body.addChild(marker);
        }

        return {
            root: root,
            alive: true,
            userControlled: userControlled,
            walkSpeed: 100.0,
            weapon: 0,
            weaponCooldown: 0,
            weaponHitTimer: 0,
            health: 5,

            update: function(dt) {

                // update texture
                if (!this.alive) {
                    body.texture = TEXTURE_PLAYER_DOWN;
                }
                else {
                    if (this.weapon === WEAPON_KEYBOARD) {
                        if (this.weaponHitTimer > 0)
                            body.texture = TEXTURE_PLAYER_HIT;
                        else
                            body.texture = TEXTURE_PLAYER_KB;
                    } 
                    else {
                        body.texture = TEXTURE_PLAYER;
                    }
                }


                if (!this.alive) {
                    return;
                }

                if (this.userControlled) {
                    this.updateUserControls(dt);
                }
                else {
                    this.updateAi(dt);
                }

                this.updateWeapon(dt);

                // player -> player collision
                for (var i in currentWorld.players) {
                    var player = currentWorld.players[i];
                    if (player == this ||
                        !player.alive) 
                        continue;
                
                    var dx = player.root.x - this.root.x;
                    var dy = player.root.y - this.root.y;
                    var dist = vec2Length(dx, dy);
                    var depth = 2*PLAYER_COLLISION_RADIUS - dist; 
                    if (depth > 0) {
                        var normX = dx / dist;
                        var normY = dy / dist;
                        player.root.x += 0.5 * normX * depth;
                        player.root.y += 0.5 * normY * depth;
                        this.root.x -= 0.5 * normX * depth;
                        this.root.y -= 0.5 * normY * depth;
                    }                    
                }
            },
            updateUserControls(dt) {
                if (this.weapon > 0) {
                    setStatusText('Left mouse button - Hit');
                }

                var movementDirX = 0;
                var movementDirY = 0;

                if (input.keyPressed(input.K_W)) {
                    movementDirY = -1;
                } 
                else if (input.keyPressed(input.K_S)) {
                    movementDirY = 1;
                }

                if (input.keyPressed(input.K_A)) {
                    movementDirX = -1;
                } 
                else if (input.keyPressed(input.K_D)) {
                    movementDirX = 1;
                }

                var len = vec2Length(movementDirX, movementDirY);
                if (len != 0) {
                    movementDirX /= len;
                    movementDirY /= len;
                }


                var deltaX = movementDirX * this.walkSpeed * dt;
                var deltaY = movementDirY * this.walkSpeed * dt;
                this.root.x += deltaX;
                this.root.y += deltaY;

                // update rotation

                this.root.rotation = Math.atan2(currentWorld.lastMouseY - this.root.y, currentWorld.lastMouseX - this.root.x);

                // pickups
                var distToClosestPickup = 999999;
                var pickupUpdateStatusText = function(){}
                var pickupOnActionKey = function(){}
                for (var i in currentWorld.keyboards) {
                    var k = currentWorld.keyboards[i];

                    var dist = vec2Dist(this.root.x, this.root.y, k.root.x, k.root.y);
                    var pickupRadius = 40;
                    if (dist < pickupRadius && dist < distToClosestPickup) {
                        (function(actionPlayer, keyboard){
                            pickupUpdateStatusText = function(){
                                setStatusText('F - Pick up keyboard');
                            };

                            pickupOnActionKey = function(){
                                keyboard.root.destroy();
                                var index = currentWorld.keyboards.indexOf(keyboard);
                                if (index > -1) {
                                    currentWorld.keyboards.splice(index, 1);
                                }

                                actionPlayer.weapon = WEAPON_KEYBOARD;

                                playSound(SOUND_PICKUP, 1);
                            };
                        })(this, k);
                    }
                }

                pickupUpdateStatusText();
                if (input.keyPressedNow(input.K_F)) {
                    pickupOnActionKey();
                }
            },
            updateAi(dt) {
                var moveX = 0;
                var moveY = 0;
                var targetRot = this.root.rotation;

                // pick up keyboards
                if (this.weapon === 0) {
                    var closestKbDist = 99999;
                    var dirToClosestKbX = 0;
                    var dirToClosestKbY = 0;

                    for (var i in currentWorld.keyboards) {
                        var k = currentWorld.keyboards[i];

                        var toKeyboardX = k.root.x - this.root.x;
                        var toKeyboardY = k.root.y - this.root.y;

                        var dist = vec2Length(toKeyboardX, toKeyboardY);
                        if (dist < closestKbDist) {
                            closestKbDist = dist;
                            dirToClosestKbX = toKeyboardX / dist;
                            dirToClosestKbY = toKeyboardY / dist;
                        }

                        var pickupRadius = 40;

                        if (dist < pickupRadius) {
                            k.root.destroy();
                            var index = currentWorld.keyboards.indexOf(k);
                            if (index > -1) {
                                currentWorld.keyboards.splice(index, 1);
                            }

                            this.weapon = WEAPON_KEYBOARD;
                            playSound(SOUND_PICKUP, 1);
                        }
                    }

                    if (closestKbDist < 500) {
                        moveX += 2*dirToClosestKbX;
                        moveY += 2*dirToClosestKbY;
                    }
                }

                

                var closestOther = null;
                var closestOtherDist = 9999;
                for (var i in currentWorld.players) {
                    var other = currentWorld.players[i];
                    if (other === this || !other.alive)
                        continue;

                    var dist = vec2Dist(this.root.x, this.root.y, other.root.x, other.root.y);

                    if (dist < closestOtherDist) {
                        closestOtherDist = dist;
                        closestOther = other;
                    }
                }

                if (closestOther != null) {
                    var dirX = (closestOther.root.x - this.root.x) / closestOtherDist;
                    var dirY = (closestOther.root.y - this.root.y) / closestOtherDist;

                    targetRot = Math.atan2(dirY, dirX);
                    
                    if (closestOtherDist < 400) {
                        if (this.weapon != 0) {
                            // attack
                            moveX += dirX;
                            moveY += dirY;
                        }
                        else {
                            // flee
                            moveX += -dirX;
                            moveY += -dirY;
                        }
                    }

                    if (closestOtherDist < 50 && this.weapon != 0) {
                        this.useWeapon();
                    }
                }

                // avoid stones:
                var tileX = Math.floor(this.root.x/TILE_SIZE);
                var tileY = Math.floor(this.root.y/TILE_SIZE);
                for (var x = -1; x <= 1; x++) {
                    for (var y = -1; y <= 1; y++) {
                        var atX = tileX + x;
                        var atY = tileY + y;
                            
                        if (atX >= 0 && atX < WORLD_WIDTH_TILES && atY >= 0 && atY < WORLD_HEIGHT_TILES &&
                            currentWorld.tileMap[tileCoordsToIndex(atX, atY)] === TILE_TYPE_STONE) {
                            
                            // just move towards the world center
                            var toCenterX = 0.5*WORLD_WIDTH_WORLDSPACE - this.root.x;
                            var toCenterY = 0.5*WORLD_HEIGHT_WORLDSPACE - this.root.y;
                            var toCenterLen = vec2Length(toCenterX, toCenterY);

                            toCenterX /= toCenterLen;
                            toCenterY /= toCenterLen;

                            targetRot = Math.atan2(toCenterX, toCenterY);
                            moveX += toCenterX;
                            moveY += toCenterY;

                            break;
                        }
                    }
                }

                // avoid walls:
                for (var i in currentWorld.walls) {
                    var w = currentWorld.walls[i];
                    
                    var fromWallX = this.root.x - w.root.x;
                    var fromWallY = this.root.y - w.root.y;
                    var length = vec2Length(fromWallX, fromWallY);

                    if (length < 70 && length > 0) {
                        fromWallX /= length;
                        fromWallY /= length;

                        moveX += 0.3*fromWallX;
                        moveY += 0.3*fromWallY;
                    }
                }

                var moveLen = vec2Length(moveX, moveY);
                if (moveLen > 0) {
                    moveX /= moveLen;
                    moveY /= moveLen;
                }

                this.root.x += moveX*this.walkSpeed*dt;
                this.root.y += moveY*this.walkSpeed*dt;
                this.root.rotation += (targetRot - this.root.rotation)*3*dt;
            },
            useWeapon() {
                if (this.weaponCooldown > 0)
                    return;

                if  (this.weapon === WEAPON_KEYBOARD) {
                    this.weaponCooldown = 0.3;
                    this.weaponHitTimer = 0.15;

                    var offset = 33;
                    var hitX = this.root.x + offset*Math.cos(this.root.rotation);
                    var hitY = this.root.y + offset*Math.sin(this.root.rotation);

                    var hitRadius = 20;
                    var damage = 1;
                    for (var i in currentWorld.players) {
                        var otherPlayer = currentWorld.players[i];
                        if (otherPlayer === this)
                            continue;
                        
                        if (vec2Dist(hitX, hitY, otherPlayer.root.x, otherPlayer.root.y) < hitRadius) {
                            otherPlayer.onHit(damage);

                            playSound(SOUND_HIT, 1);

                            var count = Math.random()*12;
                            for (var p = 0; p < count; p++)
                                currentWorld.addParticle(createKey(hitX, hitY));
                        }
                    }
                }
            },
            onHit(damage) {
                if (!this.alive)
                    return;
                this.health -= damage;

                if (this.health < 0) {
                    this.onDown();
                }
            },
            onDown() {
                this.alive = false;
                currentWorld.moveToFloorLayer(this.root);
            },
            updateWeapon(dt) {
                if (this.weaponCooldown > 0)
                    this.weaponCooldown -= dt;

                if (this.weaponHitTimer > 0) {
                    this.weaponHitTimer -= dt;
                }
            }            
        };
    }

    function createWall(tileX, tileY, length, rot) {

        var root = new PIXI.Sprite.fromImage('./gfx/wall.png');

        root.x = tilePosToWorldCentered(tileX);
        root.y = tilePosToWorldCentered(tileY);

        root.scale.y = length;

        root.anchor.x = 0.5;
        root.anchor.y = 0.5;

        var w = 10;
        var h = 64 * length;
        var x = tileX*TILE_SIZE + 0.5*TILE_SIZE - 0.5*w;
        var y = tileY*TILE_SIZE;

        if (rot !== 0) {
            root.rotation = -Math.PI*0.5;
            w = 64 * length;
            h = 10;
            x = tileX*TILE_SIZE;
            y = tileY*TILE_SIZE + 0.5*TILE_SIZE - 0.5*h;
        }

        return {
            root: root,
            x: x,
            y: y,
            width: w,
            height: h,
            rot: rot,
            health: 20
        }
    }

    function setupLevel(levelNum) {
        // clean up any previous state:
        if (currentWorld != null) {
            currentWorld.destroy();
            currentWorld = null;
        }

        // and load the new one:
        currentWorld = createWorld();
        currentWorld.levelNum = levelNum;
        app.stage.addChild(currentWorld.root);

        // frame
        for (var i = 0; i < WORLD_WIDTH_TILES; i++) {
            addStone(currentWorld, i, 0);
            addStone(currentWorld, i, WORLD_HEIGHT_TILES - 1);
        }

        for (var i = 1; i < WORLD_HEIGHT_TILES - 1; i++) {
            addStone(currentWorld, 0, i);
            addStone(currentWorld, WORLD_WIDTH_TILES - 1, i);
        }

        //levelNum = 4;

        if (levelNum === 1) {
            // walls
            currentWorld.addWall(createWall(5, 1, 1, 0));
            currentWorld.addWall(createWall(5, 2, 1, 0));
            currentWorld.addWall(createWall(5, 3, 1, 0));
            currentWorld.addWall(createWall(5, 4, 1, 0));
            currentWorld.addWall(createWall(5, 4.75, 0.5, 0));
            currentWorld.addWall(createWall(4, 5, 1, 1));
            currentWorld.addWall(createWall(4.75, 5, 0.5, 1));
            currentWorld.addWall(createWall(1, 5, 1, 1));


            currentWorld.addWall(createWall(8, 8, 1, 0));
            currentWorld.addWall(createWall(8, 7, 1, 0));
            currentWorld.addWall(createWall(8, 6, 1, 0));
            currentWorld.addWall(createWall(8, 5, 1, 0));
            currentWorld.addWall(createWall(8, 4, 1, 0));
            currentWorld.addWall(createWall(8, 4, 1, 1));
            
            currentWorld.addWall(createWall(11, 4, 1, 1));
            currentWorld.addWall(createWall(12, 4, 1, 1));



            // desks

            function deskWithKeyboard(x, y, rot) {
                var desk = createDesk(x, y, rot);
                currentWorld.addDesk(desk);
                var offset = -10;
                currentWorld.addKeyboard(createKeyboard(desk.root.x + offset*Math.cos(rot*0.5*Math.PI), desk.root.y + offset*Math.sin(rot*0.5*Math.PI), rot));
            }

            //currentWorld.addDesk(createDesk(1, 4, 2));
            deskWithKeyboard(1, 4, 2);
            deskWithKeyboard(4.5, 3, 0);
            deskWithKeyboard(4.5, 4, 0);

            deskWithKeyboard(12, 8, 0);
            deskWithKeyboard(12, 6, 0);

            // add player
            currentWorld.addPlayer(createPlayer(true, tilePosToWorldCentered(3), tilePosToWorldCentered(3)));

            // add opponents
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(11), tilePosToWorldCentered(6)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(6), tilePosToWorldCentered(5)));
        } else if (levelNum === 2) {
            // desks

            function deskWithKeyboard(x, y, rot) {
                var desk = createDesk(x, y, rot);
                currentWorld.addDesk(desk);
                var offset = -10;
                currentWorld.addKeyboard(createKeyboard(desk.root.x + offset*Math.cos(rot*0.5*Math.PI), desk.root.y + offset*Math.sin(rot*0.5*Math.PI), rot));
            }

            //currentWorld.addDesk(createDesk(1, 4, 2));
            deskWithKeyboard(4, 4, 2);
            deskWithKeyboard(4, 5, 2);
            deskWithKeyboard(4, 6, 2);
            deskWithKeyboard(3, 4, 0);
            deskWithKeyboard(3, 5, 0);
            deskWithKeyboard(3, 6, 0);

            deskWithKeyboard(7, 4, 2);
            deskWithKeyboard(7, 5, 2);
            deskWithKeyboard(7, 6, 2);
            deskWithKeyboard(6, 4, 0);
            deskWithKeyboard(6, 5, 0);
            deskWithKeyboard(6, 6, 0);

            deskWithKeyboard(10, 4, 2);
            deskWithKeyboard(10, 5, 2);
            deskWithKeyboard(10, 6, 2);
            deskWithKeyboard(9, 4, 0);
            deskWithKeyboard(9, 5, 0);
            deskWithKeyboard(9, 6, 0);

            // add player
            currentWorld.addPlayer(createPlayer(true, tilePosToWorldCentered(3), tilePosToWorldCentered(3)));

            // add opponents
            var playerWithKeyboard = createPlayer(false, tilePosToWorldCentered(7), tilePosToWorldCentered(2));
            playerWithKeyboard.weapon = WEAPON_KEYBOARD;
            currentWorld.addPlayer(playerWithKeyboard);
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(6), tilePosToWorldCentered(8)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(3), tilePosToWorldCentered(8)));
        }
        else if (levelNum === 3) {
            // desks

            function deskWithKeyboard(x, y, rot) {
                var desk = createDesk(x, y, rot);
                currentWorld.addDesk(desk);
                var offset = -10;
                currentWorld.addKeyboard(createKeyboard(desk.root.x + offset*Math.cos(rot*0.5*Math.PI), desk.root.y + offset*Math.sin(rot*0.5*Math.PI), rot));
            }

            currentWorld.addWall(createWall(4, 3, 1, 0));
            currentWorld.addWall(createWall(4, 4, 1, 0));
            currentWorld.addWall(createWall(4, 5, 1, 0));
            currentWorld.addWall(createWall(4, 6, 1, 0));
            currentWorld.addWall(createWall(4, 7, 1, 0));
            currentWorld.addWall(createWall(4, 8, 1, 0));

            deskWithKeyboard(4.5, 6, 2);
            deskWithKeyboard(4.5, 5, 2);
            deskWithKeyboard(4.5, 4, 2);
            deskWithKeyboard(4.5, 7, 2);

            currentWorld.addWall(createWall(9, 3, 1, 0));
            currentWorld.addWall(createWall(9, 4, 1, 0));
            currentWorld.addWall(createWall(9, 5, 1, 0));
            currentWorld.addWall(createWall(9, 6, 1, 0));

            deskWithKeyboard(9.5, 6, 2);
            deskWithKeyboard(9.5, 5, 2);
            deskWithKeyboard(9.5, 3, 2);

            // add player
            currentWorld.addPlayer(createPlayer(true, tilePosToWorldCentered(2), tilePosToWorldCentered(5)));

            // add opponents
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(7), tilePosToWorldCentered(2)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(6), tilePosToWorldCentered(8)));
            deskWithKeyboard(3.5, 7, 0);
            deskWithKeyboard(3.5, 8, 0);
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(2.5), tilePosToWorldCentered(8)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(10), tilePosToWorldCentered(3)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(10), tilePosToWorldCentered(7)));
        }
        else if (levelNum === 4) {
            // desks

            function deskWithKeyboard(x, y, rot) {
                var desk = createDesk(x, y, rot);
                currentWorld.addDesk(desk);
                var offset = -10;
                currentWorld.addKeyboard(createKeyboard(desk.root.x + offset*Math.cos(rot*0.5*Math.PI), desk.root.y + offset*Math.sin(rot*0.5*Math.PI), rot));
            }

            //currentWorld.addDesk(createDesk(1, 4, 2));
            deskWithKeyboard(4, 4, 2);
            deskWithKeyboard(4, 5, 2);
            deskWithKeyboard(4, 6, 2);
            deskWithKeyboard(3, 4, 0);
            deskWithKeyboard(3, 5, 0);
            deskWithKeyboard(3, 6, 0);

            deskWithKeyboard(7, 4, 2);
            deskWithKeyboard(7, 5, 2);
            deskWithKeyboard(7, 6, 2);
            deskWithKeyboard(6, 4, 0);
            deskWithKeyboard(6, 5, 0);
            deskWithKeyboard(6, 6, 0);

            deskWithKeyboard(10, 4, 2);
            deskWithKeyboard(10, 5, 2);
            deskWithKeyboard(10, 6, 2);
            deskWithKeyboard(9, 4, 0);
            deskWithKeyboard(9, 5, 0);
            deskWithKeyboard(9, 6, 0);

            deskWithKeyboard(7, 8, 1);
            deskWithKeyboard(6, 8, 1);
            deskWithKeyboard(5, 8, 1);

            // add player
            currentWorld.addPlayer(createPlayer(true, tilePosToWorldCentered(3), tilePosToWorldCentered(3)));

            // add opponents
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(7), tilePosToWorldCentered(2)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(6), tilePosToWorldCentered(7)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(3), tilePosToWorldCentered(8)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(4), tilePosToWorldCentered(2)));
            currentWorld.addPlayer(createPlayer(false, tilePosToWorldCentered(8), tilePosToWorldCentered(8)));
        }
        else {
            // game completed

            currentWorld.destroy();
            currentWorld = null;

            (function() {
                var screen = PIXI.Sprite.fromImage('gfx/outro.jpg');
                screen.x = WORLD_WIDTH_WORLDSPACE*0.5;
                screen.y = WORLD_HEIGHT_WORLDSPACE*0.5;
                screen.anchor.x = 0.5;
                screen.anchor.y = 0.5;
                screen.scale.x = 1.4;       
                screen.scale.y = 1.4;
                
                app.stage.addChild(screen);
        
                setStatusText('What a productive day!');
            })();    
        }
    }

    app.ticker.add(function(dt) {        
        if (dt > 0.033)
            dt = 0.033;

        if (currentWorld)
            currentWorld.update(dt);

        input.update();
    });

    input.init();

    // intro:
    (function() {
        var screen = PIXI.Sprite.fromImage('gfx/intro.jpg');
        screen.x = WORLD_WIDTH_WORLDSPACE*0.5;
        screen.y = WORLD_HEIGHT_WORLDSPACE*0.5;
        screen.anchor.x = 0.5;
        screen.anchor.y = 0.5;
        screen.scale.x = 1.4;       
        screen.scale.y = 1.4;       

        var done = false;
        screen.pointerdown = function() {
            if (done)
                return;
            done = true;

            screen.destroy();

            setupLevel(1);        
        }
        screen.interactive = true;
        
        app.stage.addChild(screen);

        setStatusText('Left mouse button - continue');
    })();    
})();