var sound = (function soundModule(){
    "use strict"
    
    var visible = true;
    var visibilityVol = 1.0;
    var masterVol = 1.0;
    var sounds = {};
    var loops = {};
    
    var extensions = ['.wav'];
    
    function updateVol() {
        Howler.volume(masterVol * visibilityVol);
    }
    
    function onPageShow() {
        if (visible) return;
        
        visible = true;
        
        visibilityVol = 1.0;
        updateVol();
    }
    
    function onPageHide() {
        if (!visible) return;
        
        visible = false;
        
        visibilityVol = 0.0;
        updateVol();
    }
    
    function onVisibilityChange() {
        if (document.hidden) {
            onPageHide();
        }
        else {
            onPageShow();
        }
    }
    
    function init() {
        publicApi.supported = window.AudioContext || window.webkitAudioContext;
        
        if (document.hidden == "undefined")
        {
            window.addEventListener("pageshow", onPageShow, false);
            window.addEventListener("pagehide", onPageHide, false);
            window.addEventListener("focus", onPageShow, false);
            window.addEventListener("blur", onPageHide, false);
        }
        else
        {
            document.addEventListener("visibilitychange", onVisibilityChange, false);
            onVisibilityChange();
        }
    }
    
    function load(pathWoExtension, completionHandler) {
        
        if (!publicApi.supported) {
            completionHandler();
            return;
        }
        
        var s = new Howl({
            src: extensions.map(function(ext) {
                return pathWoExtension + ext;
            }),
            onload: function() {
                sounds[pathWoExtension] = s;
                
                completionHandler();
            },
            onloaderror: function(id, error) {                
                completionHandler();
            }
        });
    }
    
    function playImpl(path, vol, rate, loop, outDetails) {
        var s = sounds[path];
        if (!s) {
            return null;
        }
        

        var originalMasterVol;
        if (loop) {
            // howler will ignore play call if the master volume is 0, as a workaround we temporarily enable sound while the loop starts 
            originalMasterVol = Howler.volume();
            Howler.volume(1);
        }

        var id = s.play();
        s.volume(vol, id);
        s.rate(rate, id);
        s.loop(loop, id);

        if (loop) {
            // restore original master volume
            Howler.volume(originalMasterVol);
        }
        
        if (outDetails) {
            outDetails.s = s;
            outDetails.id = id;
        }
        
        return outDetails;
    }
    
    function playEx(path, vol, rate, loop) {
        if (!publicApi.supported)
            return;

        var details = playImpl(path, vol, rate, false);
    }
    
    function loop(path, opts) {
        if (!publicApi.supported)
            return function(){};

        var rate = (opts && opts.rate) || 1.0;
        var vol = (opts && opts.vol) || 1.0;
        
        var details = playImpl(path, vol, rate, true, {}); 

        if (details) {
            loops[path] = details;
        }
        
        var stopped = false;
        return function stopLoop() {
            if (stopped) return;
            stopped = true;
            
            if (details) {
                details.s.stop(details.id);
                if (loops[path].id === details.id) {
                    loops[path] = null;
                }
            }
        };
    }

    function setLoopVolume(path, vol) {
        var details = loops[path];
        if (!details)
            return;
        
        details.s.volume(vol, details.id);
    }
    
    function play(path) {
        if (!publicApi.supported)
            return;

        playImpl(path, 1.0, 1.0, false);
    }
    
    function setMasterVol(vol) {
        masterVol = vol;
        
        updateVol();
    }

    function muteAllExcept(exceptions, mute) {
        for (var path in sounds) {
            if (exceptions.indexOf(path) === -1) {
                sounds[path].mute(mute);
            }
        }
    }
    
    function muteOnly(exceptions, mute) {
        for (var path in sounds) {
            if (exceptions.indexOf(path) != -1) {
                sounds[path].mute(mute);
            }
        }
    }

    var publicApi = {
        supported: null,
        
        init: init,
        load: load,
        playEx: playEx,
        play: play,
        loop: loop,
        setMasterVol: setMasterVol,
        setLoopVolume: setLoopVolume,
        muteAllExcept: muteAllExcept,
        muteOnly: muteOnly
    };    
    return publicApi;
})();