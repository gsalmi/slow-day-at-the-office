function vec2Length(x, y) {
    return Math.sqrt(x*x + y*y);
}