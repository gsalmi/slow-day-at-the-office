var input = (function(){
    
    var pressed = [];
    var pressedNow = [];

    function removeAll(arr, x) {
        var index = 0;
        while (true) {
            index = arr.indexOf(x, index);
            if (index === -1)
                return;
            
            arr.splice(index, 1);
        }
    }
    
    function shouldPreventDefault(keyCode) {
        switch (keyCode) {
            case publicApi.K_UP:
            case publicApi.K_DOWN:
            case publicApi.K_LEFT:
            case publicApi.K_RIGHT:
                return true;
            default:
                return false;
        }
    }

    function onKeyDown(evt) {
        var code = evt.keyCode;

        if (shouldPreventDefault(code))
            evt.preventDefault();        
        
        if (pressed.indexOf(code) != -1)
            return;
        
        pressedNow.push(code);
        pressed.push(code);
    }
    
    function onKeyUp(evt) {
        if (shouldPreventDefault(evt.keyCode))
            evt.preventDefault();
        
        removeAll(pressed, evt.keyCode);
    }
    
    var publicApi = {
        init: function() {
            var w = window;

            w.document.addEventListener('keydown', onKeyDown);
            w.document.addEventListener('keyup', onKeyUp);
        },
        update: function() {
            pressedNow.length = 0;
        },
        
        keyPressed: function(code) {
            return pressed.indexOf(code) != -1;
        },
        keyPressedNow: function(code) {
            return pressedNow.indexOf(code) != -1;
        },
        
        K_LEFT: 37,
        K_UP: 38,
        K_RIGHT: 39,
        K_DOWN: 40,
        K_SPACE: 32,
        K_PAGE_UP: 33,
        K_PAGE_DOWN: 34,
        K_W: 87,
        K_A: 65,
        K_S: 83,
        K_D: 68,
        K_F: 70,
        K_R: 82
    };
    return publicApi;
})();